# Binar Challange 5 - Rental Car Binar

---

This Challange made by Ira Dwi Nugrayani.

## Feature

- Create API for insert new car data
- Create API for update car data
- Create API for delete car data
- Create API for show all cars data
- Create view to show all cars
- Create form to add or edit a car data

## How to install

1. install all required dependencies with command

properties
    yarn install


2. Create the database with command below

properties
    yarn sequelize db:create


3. Migrate all data in database with command bellow

properties
    yarn sequelize db:migrate


4. Run the server.

properties
    yarn dev


There are several servers with different functions, the first server is accessed with `yarn start`, the basic server needs to be restarted every time. `yarn dev`, a server that automatically restarts if there is a modification to the \*.js file and a `yarn watch` server that will restart when there are changes to a static file or a file in the public directory.

## Database Structure

| Column    | Type                     |
| --------- | ------------------------ |
| id        | integer                  |
| name      | character(255)           |
| price     | float                    |
| size      | character(255)           |
| foto      | character(255)           |
| createdAt | timestamp with time zone |
| updatedAt | timestamp with time zone |